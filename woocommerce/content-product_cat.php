<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<li <?php wc_product_cat_class( '', $category ); ?>>
	<?php
	/**
	 * woocommerce_before_subcategory hook.
	 *
	 * @hooked woocommerce_template_loop_category_link_open - 10
	 */
	do_action( 'woocommerce_before_subcategory', $category );

	/**
	 * woocommerce_before_subcategory_title hook.
	 *
	 * @hooked woocommerce_subcategory_thumbnail - 10
	 */
	//do_action( 'woocommerce_before_subcategory_title', $category );

	// CHECK FOR BRANDS
	if(empty(get_query_var('brand'))){

		// get product thumbnail
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 1,
			'orderby' => 'rand',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_cat',
					'field' => 'term_id',
					'terms' => $category->term_id
				)
			)
		);
		$product = new WP_Query($args);
		if($product->posts[0]){
			$product_obj = $product->posts[0];
			$img_url = get_the_post_thumbnail_url($product_obj->ID, 'full');
			echo '<div class="product-cat-thumb" style="background:url('.$img_url.') center center no-repeat;background-size:contain;">';
		}
	}else {
		// get brand thumbnails
		$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
		$img_url = wp_get_attachment_url( $thumbnail_id );
		if($img_url){
			echo '<div class="product-brand-thumb">';
				echo '<img src="'.$img_url.'" />';
		}else {
			echo '<div class="product-brand-thumb no-logo">';
		}

	}

	echo '<div class="product-cat-overlay">';
	/**
	 * woocommerce_shop_loop_subcategory_title hook.
	 *
	 * @hooked woocommerce_template_loop_category_title - 10
	 */
	//do_action( 'woocommerce_shop_loop_subcategory_title', $category );
	echo '<h3>'.$category->name.'</h3>';
	echo '</div>';
	echo '</div>';
	/**
	 * woocommerce_after_subcategory_title hook.
	 */
	do_action( 'woocommerce_after_subcategory_title', $category );

	/**
	 * woocommerce_after_subcategory hook.
	 *
	 * @hooked woocommerce_template_loop_category_link_close - 10
	 */
	do_action( 'woocommerce_after_subcategory', $category ); ?>
</li>