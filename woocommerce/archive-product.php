<?php get_header(get_field('site_header', 'option')); ?>

	<section id="main" role="main">
		<div class="content-row">
			<div class="woo-grid wrapper">
				<?php
					/**
					 * The Template for displaying product archives, including the main shop page which is a post type archive.
					 *
					 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
					 *
					 * @author 		WooThemes
					 * @package 	WooCommerce/Templates
					 * @version     2.0.0
					 */

					if ( ! defined( 'ABSPATH' ) ) {
						exit; // Exit if accessed directly
					}

					/**
					 * woocommerce_before_main_content hook
					 *
					 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
					 * @hooked woocommerce_breadcrumb - 20
					 */

					///// CUSTOM HOOK IN THEME FUNCTIONS FOR THIS
					do_action( 'woocommerce_before_main_content' );

				?>

				<?php //do_action( 'woocommerce_archive_description' ); ?>

				<?php if ( have_posts() ) : ?>


					<?php
						/**
						 * woocommerce_before_shop_loop hook
						 *
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						// do_action( 'woocommerce_before_shop_loop' );
					?>	
					<?php if(woocommerce_products_will_display()) : ?>
						<div class="woo-result-count">
							<?php woocommerce_result_count(); ?>

							<div class="woo-sort-by">
								<strong>SORT BY</strong>

								<?php 		
									// get order/orderby
									$order = $_GET["order"] ? sanitize_text_field($_GET["order"]) : false;
									$orderby = $_GET["orderby"] ? sanitize_text_field($_GET["orderby"]) : false;
								?>

								<span class="sort-by-price">
									<?php if($order == 'asc' && $orderby == 'price') : ?>
										<a href="?<?php echo http_build_query(array_merge($_GET, array("orderby" => "price", "order" => "desc"))); ?>">Price <i class="bts bt-caret-up"></i></a>
									<?php else : ?>
										<a href="?<?php echo http_build_query(array_merge($_GET, array("orderby" => "price", "order" => "asc"))); ?>">Price 
											<?php if($orderby == 'price') : ?>
												<i class="bts bt-caret-down"></i>
											<?php endif; ?>
										</a>
									<?php endif; ?>
								</span>

								<span class="sort-by-name">
									<?php if($order == 'asc' && $orderby == 'name') : ?>
										<a href="?<?php echo http_build_query(array_merge($_GET, array("orderby" => "name", "order" => "desc"))); ?>">Name <i class="bts bt-caret-up"></i></a>
									<?php else : ?>
										<a href="?<?php echo http_build_query(array_merge($_GET, array("orderby" => "name", "order" => "asc"))); ?>">Name 
											<?php if($orderby == 'name') : ?>
												<i class="bts bt-caret-down"></i>
											<?php endif; ?>
										</a>
									<?php endif; ?>
								</span>

							</div>
						</div>
					<?php endif; ?>

					<?php woocommerce_product_loop_start(); ?>

						<?php woocommerce_product_subcategories(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php wc_get_template_part( 'content', 'product' ); ?>

						<?php endwhile; // end of the loop. ?>

					<?php woocommerce_product_loop_end(); ?>

					<?php
						/**
						 * woocommerce_after_shop_loop hook
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
					?>

				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<?php wc_get_template( 'loop/no-products-found.php' ); ?>

				<?php endif; ?>

				<?php
					/**
					 * woocommerce_after_main_content hook
					 *
					 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					do_action( 'woocommerce_after_main_content' );
				?> 

				<?php
					/**
					 * woocommerce_sidebar hook
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					//do_action( 'woocommerce_sidebar' );
				?>

				<?php //get_footer( 'shop' ); ?>

				<?php //get_sidebar(); ?>
				<span class="clearer"></span>
			</div>
		</div>
	</section>
	<!-- /section -->
	
<?php get_footer(get_field('site_footer', 'option')); ?>
