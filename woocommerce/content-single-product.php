<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<?php 
	// get other product terms
	$product_cats = wp_get_post_terms($post->ID, 'product_cat');
	$parent_cats = array();
	if($product_cats){
		foreach($product_cats as $cat){
			if(!$cat->parent){
				$parent_cats[] = $cat->term_id;
			}
		}
	}
?>


<div class="woo-product-primary">
	<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php if($parent_cats){post_class('woo-product');}{post_class('woo-product no-cats');} ?>>

		<div class="woo-product-title">
			<?php woocommerce_template_single_title(); ?>
			<?php 
				// get meta
				$brand_sku = array();
				if(taxonomy_exists('product_brand') && $brands = wp_get_post_terms($post->ID, 'product_brand')){
					$brand_sku[] = 'by '. $brands[0]->name;
				}
				if($sku = get_post_meta($post->ID, '_sku', true)){
					$brand_sku[] = 'Item # '. $sku;
				}
			?>
			<?php if($brand_sku) : ?>
				<span class="woo-item-number"><?php echo implode('<span>|</span>', $brand_sku); ?></span>
			<?php endif; ?>
		</div>

		<div class="woo-product-images">
			<?php
				/**
				 * woocommerce_before_single_product_summary hook.
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
			?>
		</div>


		<div class="summary entry-summary">

			<?php
				/**
				 * woocommerce_single_product_summary hook.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 */
				// do_action( 'woocommerce_single_product_summary' );
			?>

			<?php if($post->post_excerpt) : ?>
				<div class="woo-product-description">
					<?php woocommerce_template_single_excerpt(); ?>
				</div>
			<?php endif; ?>

			<div class="woo-add-to-cart">
				<?php woocommerce_template_single_price(); ?>
				<?php woocommerce_template_single_add_to_cart(); ?>
			</div>

		</div><!-- .summary -->

		<?php
			/**
			 * woocommerce_after_single_product_summary hook.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
			do_action( 'woocommerce_after_single_product_summary' );
		?>

		<meta itemprop="url" content="<?php the_permalink(); ?>" />

	</div><!-- #product-<?php the_ID(); ?> -->


	<?php if($parent_cats) : ?>
		<div class="woo-sidebar">
			<div class="product-cats">
				<h4>More</h4>
				<ul class="product-cat-list">
				<?php foreach($product_cats as $cat) : ?>
					<?php // parent cats only ?>
					<?php if(!$cat->parent) : ?>
						<li><a href="/product-category/<?php echo $cat->slug; ?>/"><?php echo $cat->name; ?></a></li>
						<?php $parent_cats[] = $cat->term_id; ?>
					<?php endif; ?>
				<?php endforeach; ?>
				</ul>
			</div>

			<?php 
				// check for brands that have products in related parent categories
				$product_brands = array();
				if($parent_cats){

					$args = array(
						'posts_per_page' => -1,
						'post_type' => 'product',
						'tax_query' => array(
							array(
								'taxonomy' => 'product_cat',
								'field' => 'term_id',
								'terms' => $parent_cats
							)
						)
					);
					$cat_posts = new WP_Query($args);

					foreach($cat_posts->posts as $cat_post){
						$brands = wp_get_post_terms($cat_post->ID, 'product_brand');
						
						if($brands){
							foreach($brands as $brand){
								if(!array_key_exists($brand->term_id, $product_brands)){
									$product_brands[$brand->term_id] = $brand;
								}
							}
						}
					}
				}
			?>
			<?php if(taxonomy_exists('product_brand') && $product_brands) : ?>
				<div class="product-brands">
					<h4>Brands</h4>
					<ul class="product-brand-list">
					<?php foreach($product_brands as $brand) : ?>
						<li><a href="/brand/<?php echo $brand->slug; ?>/"><?php echo $brand->name; ?></a></li>
					<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
		
		</div>
	<?php endif; ?>
</div>
</div>
</div>
<div class="woo-related">
	<?php woocommerce_output_related_products(); ?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
