<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="woo-dashboard-text">
	<h3>Hello <strong><?php echo $current_user->display_name; ?></strong></h3>
	<p>From your account dashboard you can view your recent orders, manage your shipping and billing addresses and edit your password and account details. Problems? <a href="/contact">Contact us</a>, we'll be happy to help!</p>
</div>

<div class="woo-dashboard-buttons">
	<h4>What would you like to do?</h4>
	<a class="button" href="<?php echo wc_get_endpoint_url('orders'); ?>">RECENT ORDERS</a>
	<a class="button" href="<?php echo wc_get_endpoint_url('edit-address'); ?>">UPDATE ADDRESSES</a>
	<a class="button" href="<?php echo wc_get_endpoint_url('edit-account'); ?>">UPDATE PASSWORD</a>
</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0 
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );
?>
