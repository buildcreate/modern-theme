		
		<footer id="footer" class="footer-2" role="contentinfo">
			<?php global $bc_flex_content; $bc_flex_content->add_layouts_to_footer(); ?>
			<?php global $mo_options; global $phpcolor; ?>		
			<div class="footer-bottom <?php if($phpcolor->isDark($mo_options->theme_options['footer_background_color'])) {echo 'dark';} ?>">			
				<div class="wrapper">					
					<div class="footer-copyright">
						Copyright <?php echo date('Y'); ?> <?php echo get_field('name', 'option'); ?>
					</div>
					<div class="footer-credit">
						<?php $currentDomain = preg_replace('/www\./i', '', $_SERVER['SERVER_NAME']); ?>
						<a target="_BLANK" href="https://buildcreate.com/?utm_source=<?php echo $currentDomain; ?>&utm_campaign=footer_links&utm_medium=footer_link" title="Michigan Web Design">Michigan Web Design</a> by <a target="_BLANK" href="https://buildcreate.com/?utm_source=<?php echo $currentDomain; ?>&utm_campaign=footer_links&utm_medium=footer_link" title="Michigan Web Design">build/create</a>
					</div>
					<span class="clearer"></span>
				</div>
			</div>
		</footer>
	
		</div>
		<?php wp_footer(); ?>
	</body>
</html>