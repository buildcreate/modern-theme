<?php
	
	add_action( 'wp_ajax_mo_ajax_remove_from_cart', 'mo_ajax_remove_from_cart' );
	add_action( 'wp_ajax_nopriv_mo_ajax_remove_from_cart', 'mo_ajax_remove_from_cart' );
	
	function mo_ajax_remove_from_cart() {
		global $woocommerce;
		
		$cart = $woocommerce->cart;

		$cart->remove_cart_item($_POST['product_id']);
		
		echo $cart->cart_contents_count;
		
		die();
	}
	

	