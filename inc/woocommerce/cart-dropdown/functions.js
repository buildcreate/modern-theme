jQuery(document).ready(function($){
	
	$('.ajax-remove-item').on('click', function(e){
		
		e.preventDefault();
		
        var product_id = $(this).attr("rel");
        
        $.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            data: { action: "mo_ajax_remove_from_cart", 
                    product_id: product_id
            },success: function(data){
                console.log(product_id+' Removed');
					$('#'+product_id).fadeOut();
					$('#'+product_id).remove();
					$('#cart-qty').html('('+data+')')
					
					if(data == 0) {
						$('#header .cart_list').html('<li style="text-align: center;">Your Cart is Empty!</li>');
					}
					
            }
        });
        
        return false;
        
    });
    
});