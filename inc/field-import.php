<?php 
	
if( function_exists('acf_add_local_field_group') ):

	include_once('defs/header-options.php');
	include_once('defs/general-options.php');
	include_once('defs/footer-options.php');
	include_once('defs/address-phone.php');

	// Page Header options
	$json = trim(file_get_contents(get_template_directory() . '/inc/defs/page-options.json'), '[]');
	$json_arr = json_decode($json, true);

	// maybe modify which post types content rows display on
	$post_types = get_field('post_types_using_content_rows', 'option');
	if($post_types){

		// add in the selected types (duplicates are fine)
		foreach($post_types as $type){
			$json_arr['location'][] = array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => $type,
				)
			);
		}
	}

	// add page header options
	acf_add_local_field_group($json_arr);
	
	include_once('defs/site-options-social.php');

endif;
