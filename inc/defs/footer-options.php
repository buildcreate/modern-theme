<?php if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_57602f3fd10fa',
	'title' => 'Theme Options: Footer',
	'fields' => array (
		array (
			'default_value' => '#333',
			'key' => 'field_57602f4728f99',
			'label' => 'Background Color',
			'name' => 'footer_background_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		),
		array (
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'key' => 'field_5764243b50583',
			'label' => 'Name',
			'name' => 'name',
			'type' => 'text',
			'instructions' => 'For copyright info',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'layout' => 'vertical',
			'choices' => array (
				'default' => 'Default',
				2 => 'Footer 2',
			),
			'default_value' => '',
			'other_choice' => 0,
			'save_other_choice' => 0,
			'allow_null' => 0,
			'return_format' => 'value',
			'key' => 'field_5875417ae13d7',
			'label' => 'Site Footer',
			'name' => 'site_footer',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		),
		array (
			'default_value' => 0,
			'message' => '',
			'ui' => 1,
			'ui_on_text' => '',
			'ui_off_text' => '',
			'key' => 'field_5889170d22cd8',
			'label' => 'Add Social Icons to Footer?',
			'name' => 'add_to_footer',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5875417ae13d7',
						'operator' => '==',
						'value' => 'default',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'theme-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;