<?php
	
	class ModernOrgOptions {
		
		public $theme_options;
		public $site_options;
		public $page_options;
				
		function __construct() {

			// Build arrays of the fields we want to store in this class
			
			$theme_options = array(
				 'site_max_width',
				 'default_page_header_background',
				 'transparent_navigation_bar',
				 'nav_bar_opacity',
				 'increase_navigation_contrast',
				 'primary_dark',
				 'primary_light',
				 'secondary_dark',
				 'secondary_light',
				 'button_style',
				 'logo',
				 'header_background_color',
				 'navigation_link_color',
				 'navigation_link_hover_color',
				 'footer_background_color',
				 'font',
				 'sticky_header',
				 'utility_nav_background',
				 'use_utility_nav',
				 'include_search',
				 'include_login',
				 'include_contact_info',
				 'include_social',
				 'use_menu',
				 'use_menu_on_left',
			);
			
			$site_options = array(
				'address_1',
				'address_2',
				'city',
				'state',
				'zipcode',
				'contact_phone',
				'contact_email',
				'social_icons',
			);
			

			
			
			// Loops to assign all of these to their class properties
			
			foreach($theme_options as $o) {
				$this->theme_options[$o] = get_field($o, 'option');
			}
			
			foreach($site_options as $o) {
				$this->site_options[$o] = get_field($o, 'option');
			}
			
			// Done assigning options to class properties!				
						
		}
		

		// no longer used, all defined in header logic
		function init_post_options() {
			
			$page_options = array(
				'header_background',
				'header_height',
				'color_overlay',
				'overlay_opacity',
				'header_title',
				'header_content',
				'include_cta',
				'button_text',
				'button_url',
				'header_type',
				'content_position',
				'content_width',
				'hide_content_on_mobile',
				'use_sidebar',
				'background_image',
				'title_alignment',
				'constrain_content_to_site_width',
			);
			
			// Just in case we use it in the loop
			wp_reset_query();			
			
			global $post;
			
			
			
			if(is_home()) {
				$post = get_post(get_option( 'page_for_posts' ));
			}
			

			
			if(is_page() || is_front_page() || is_singular('service') || is_singular('case_study') || is_home()) {
				foreach($page_options as $o) {
					$this->page_options[$o] = get_field($o, $post->ID);
				}	
			}
			
		}
		
		function constrain_hero() {
			if($this->page_options['constrain_content_to_site_width']) {
				return 'class="header-width" style="max-width: '.$this->theme_options['site_max_width'].';"';
			}
		}
		
	}