(function($){
	acf.add_filter('validation_complete', function(json, $form){
		
		// only if errors
		if(json.errors){
			
			// check if hidden by tab
			var errors = json.errors;
			var hidden = false;
			var hidden_message = false;
			$.each(errors, function(i, item){
				if($('input[name="'+item.input+'"]').closest('.hidden-by-tab').length){
					hidden = true;
					return false;
				}
			});

			// check if message already exists
			if($('.acf-tab-hidden').length){
				hidden_message = true;
			}

			// remove if exists but not necessary now
			if(!hidden && hidden_message){
				$('.acf-tab-hidden').remove();
			}

			// prepend the message if needed
			if(hidden && !hidden_message){
				$message = $('<div class="acf-error-message acf-tab-hidden" style="background:#dd4232;border-left: #F55E4F solid 4px;"><span style="display:block; font-size: 13px !important; line-height: 1.5; margin: 0.5em 0; padding: 2px; text-shadow: none; color: #fff;">NOTICE: Invalid fields are hidden by tabs.</span><a href="#" class="acf-icon -cancel small"></a></div>');
				$form.prepend($message);
			}
		}
	
		return json;	
	});
})(jQuery);