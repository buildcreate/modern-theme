
jQuery(document).ready(function($){
	// check window size for whether or not to show scroll to top
	var width = $(window).width();
	if(window.innerWidth){
		$(window).resize(function(){
			width = window.innerWidth;
		});
	}
	else{
		$(window).resize(function(){
			width = $(window).width();
		});
	}
	// scroll to top 
	$(window).scroll(function() {
		if(width >= 960){
			if($(this).scrollTop() != 0){$('#top-link').stop().fadeTo('fast', 1);}
			else{$('#top-link').stop().fadeTo('fast', 0);}
		}else{
			$('#top-link').hide();
		}
	});
	$('#top-link').click(function() {
		$('body,html').animate({scrollTop:0},'slow',"easeInOutQuart");
	});	
	

	// check for admin bar to format header and menu
	if($('#wpadminbar').length){
		$('body').addClass('has-wpadminbar');
	}
	
	if($('#header-slider')) {
		$('#header-slider').slick({
			autoplaySpeed: 4000,
			autoplay: false,
			arrows: true,
			dots: false,
			draggable: true
		});
	}
	
	$('#header-slider .slick-arrow').removeAttr('style');

	// nav submenus
	$('.main-nav .sub-menu .sub-menu').hide();
	$('.main-nav .sub-menu .menu-item-has-children>a').on('click', function(e){
		e.preventDefault();
		$(this).toggleClass('open');
		$(this).next('ul').slideToggle('fast');
	});
	
	
	// Scroll functions!
	$(window).on('scroll', function(){	
		// Header shrinkage		
		if($(window).scrollTop() > $('#header').height() && $('.sticky').length) {
			$('#header').addClass('skinny');
		} else if ($(window).scrollTop() < $('#header').height() && $('.sticky').length) {
			$('#header').removeClass('skinny');
		}
		
	});
	
	// Height adjustments for different header types / admin	
	
	function fullScreenSplash(el, targ, headerHeight, adminbarHeight) {
		if (el.height() == $(window).height()) {
			
			if($('#header').hasClass('transparent') && !$('#header').hasClass('sticky')) {headerHeight = 0;}
			if($('#header').hasClass('transparent') && $('#header').hasClass('sticky')) {headerHeight = 0;}
			
			oldHeight = el.height();
			newHeight = oldHeight - headerHeight - adminbarHeight;
			targ.css({'maxHeight': newHeight+'px'})
			
		}
	}
	
	
	
	function setHeaderHeight() {
		
		headerHeight = $('#header').height();
		adminbarHeight = $('#wpadminbar').height();
		
		fullScreenSplash($('#header-slider'), $('#header-slider .slick-slide'), headerHeight, adminbarHeight);
		fullScreenSplash($('#header-hero'), $('#header-hero'), headerHeight, adminbarHeight);
		
		if($('#header').hasClass('sticky')) {
		
			if(!$('#header').hasClass('transparent')) {
				
				if ($('#header-slider')) {
					
					$('#header-slider').css({'marginTop':headerHeight+'px'});
																
				}
				
				if ($('#header-hero')) {
					
					$('#header-hero').css({'marginTop':headerHeight+'px'});
														
				}	
				
			}			
		} else {
	
			if($('#header').hasClass('transparent')) {
				if ($('#header-slider')) {

					$('#header-slider').css({'marginTop':-headerHeight+'px'});							
				}
				
				if ($('#header-hero')) {
					
					$('#header-hero').css({'marginTop':-headerHeight+'px'});
										
				}			
			}
		}				
	}
		
	$(window).load(function() {
		setHeaderHeight();
	});	

	$(window).on('resize', function() {
		setHeaderHeight();
	});
	
	// Mobile utility nav
	$('#drawer-handle').on('click', function(e) {
		
		$('.drawer').toggleClass('open');
		
		$('.drawer').animate({
			height: 'toggle'
		});
				
	});
	
	// // auto background text coloring
	// backgrounds = new Array;
	// image_boxes = new Array;
	
	// // row backgounds
	// if($('.has-image.content-row').length > 0) {
	// 	backgrounds.push('.has-image.content-row');
	// 	image_boxes.push('.has-image.content-row');
	// }	

	// // wrapper backgrounds
	// if($('.has-image.wrapper').length > 0) {
	// 	backgrounds.push('.has-image.wrapper');
	// 	image_boxes.push('.has-image.wrapper');
	// }

	// if($('.hero-content').length > 0) {
	// 	backgrounds.push('.hero-content');
	// }
	
	// if($('#header-background-image').length > 0) {
	// 	image_boxes.push('#header-background-image');
	// }

	// if(backgrounds.length > 0 && image_boxes.length > 0) {
		
	// 	// Content background darkness check
	// 	BackgroundCheck.init({
	// 		targets: backgrounds.join(),
	// 		images : image_boxes.join(),
	// 		minOverlap : 75,
	// 		threshold: 50,
	// 		windowEvents: false
	// 	});
	// }
	
	// $('.content-wysiwyg table').stackcolumns();
});