<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		
		<link href="<?php echo CHILD_DIR; ?>/favicon.png" rel="shortcut icon">
		
		<?php wp_head(); ?>
		
		<?php if(is_user_logged_in() && !current_user_can('administrator')) { ?>
			<script>
				jQuery(document).ready(function($) {
					if($('#wpadminbar') && $('#header').hasClass('sticky')) {
						adminBarHeight = $('#wpadminbar').height();
						$('#header').css('top',adminBarHeight+'px');
					}
				});
			</script>
		<?php } ?>
	</head>

	<body <?php body_class(); ?>>
		<div class="bg-overlay">
			<a class="hidden" href="#main">Skip to content</a>
			<div id="top-link"><i class="btl bt-angle-up"></i><span>TOP</span></div>
			<?php global $mo_options; ?>

			<header id="header" role="banner" class="header-2 <?php echo modern_header_class(); ?>">
				<?php if($mo_options->theme_options['use_utility_nav']) { ?>
				<div class="drawer">
					<ul class="utility-nav">
						<div class="wrapper">						
							<div class="left">
								<ul>
								<?php do_action('mo_before_left_utility'); ?>
								<?php echo mo_header_contact(); ?>
								<?php if($mo_options->theme_options['use_menu']) {
											echo '<li class="utility-right-nav">'.$mo_options->theme_options['use_menu_on_left'].'</li>';
										}
								?>
								<?php do_action('mo_after_left_utility'); ?>
								</ul>
							</div>
							
							<div class="right">
								<?php global $mo_options; do_action('mo_before_right_utility'); ?>
								
							<?php if($mo_options->theme_options['include_login']) { ?>
								<?php if(is_user_logged_in()) : ?>
									<li><a href="<?php echo wp_logout_url(home_url()); ?>" title="login">Logout</a></li>
								<?php else : ?>
									<li><a href="<?php echo wp_login_url($_SERVER['REQUEST_URI']); ?>" title="login">Login</a></li>
								<?php endif; ?>					
							<?php } ?>
							
							<?php if(class_exists('woocommerce')) { ?>
								<?php if(is_user_logged_in()) : ?>
									<li class="pipe"><a href="/my-account/" title="profile">My Account</a></li>
								<?php endif; ?>
							<li class="cart pipe">
								<a href="<?php bloginfo('url'); ?>/cart" title="View Cart">
									(<?php global $woocommerce; echo $woocommerce->cart->cart_contents_count; ?>) Cart <i class="bts bt-shopping-cart"></i>
								</a>
							</li>
							<?php } ?>
								
								<?php if($mo_options->theme_options['use_menu']) {
											echo '<li class="utility-right-nav">'.$mo_options->theme_options['use_menu'].'</li>';
										}
								?>
								<?php do_action('mo_after_right_utility'); ?>
							</div>	
						</div>
					</ul>
				</div>
				<div id="drawer-handle">Contact & Login <i class="btr bt-angle-down"></i></div>
				<?php } ?>
				

				<div class="wrapper">

					<div class="logo">
						<?php global $mo_options; if(is_page('home')) : ?>
							<?php if($mo_options->theme_options['logo']) : ?>
								<img src="<?php echo $mo_options->theme_options['logo']; ?>" alt="Logo" class="logo-img">
							<?php else : ?>
								<img src="<?php echo bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" class="logo-img" />
							<?php endif; ?>
						<?php else : ?>
							<a href="<?php echo home_url(); ?>">
								<?php if($mo_options->theme_options['logo']) : ?>
									<img src="<?php echo $mo_options->theme_options['logo']; ?>" alt="Logo" class="logo-img">
								<?php else : ?>
									<img src="<?php echo bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" class="logo-img" />
								<?php endif; ?>
							</a>
						<?php endif; ?>
					</div>

					<div class="secondary-header-container">
						<?php if($cc = get_field('we_accept', 'option')) : ?>
							<div class="we-accept">
								<a href="/contact/">Contact Us</a>
								<span class="cc-logos">We Accept 
									<?php if(in_array('mastercard', $cc)) : ?>
										<i class="fab fab-cc-mastercard"></i>
									<?php endif; ?>
									<?php if(in_array('visa', $cc)) : ?>
										<i class="fab fab-cc-visa"></i>
									<?php endif; ?>
									<?php if(in_array('amex', $cc)) : ?>
										<i class="fab fab-cc-amex"></i>
									<?php endif; ?>
									<?php if(in_array('discover', $cc)) : ?>
										<i class="fab fab-cc-discover"></i>
									<?php endif; ?>
								</span>
							</div>
						<?php endif; ?>

						<?php if($search_page = get_field('product_search_page', 'option')) : ?>
							<div class="product-search">
								<form method="GET" action="<?php echo $search_page; ?>">
									<input type="text" name="q" placeholder="Search Item #'s and Keywords" />
									<button><i class="btb bt-search"></i></button>
								</form>
							</div>
						<?php endif; ?>
					</div>


					<?php // mobile nav handle ?>
					<div class="nav-handle-wrap">
						<a class="nav-handle" href="#mobile-nav"><i class="btr bt-bars"></i></a>
					</div>
				</div>

				<div class="nav-container">
					<div class="wrapper">
						<nav class="main-nav" role="navigation">
							<ul> 
								<?php wp_nav_menu(array('theme_location' => 'header', 'items_wrap' => '%3$s', 'container'=> false)); ?>
								<?php do_action('mo_inside_nav'); ?>
							</ul> 
						</nav>
					</div>
				</div>

				<?php do_action('mo_after_nav'); ?>
			</header>

			<?php do_action('mo_after_header'); ?>