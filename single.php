<?php get_header(get_field('site_header', 'option')); ?>
	
<?php do_action('mo_render_header', $mo_options); ?>
	
	<section id="main" class="blog single" role="main">
		
		<div class="wrapper">
			<div class="content-wrap has-sidebar">
				
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
						<div class="post-thumb">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail(); ?>
							</a>
						</div>
						<?php the_content(); ?>
						
					</article>
					
					<div class="pagination">	
						<div class="alignleft"><?php previous_post_link(); ?></div> 
						<div class="alignright"><?php next_post_link(); ?></div>
					</div>
					
				<?php endwhile; ?>
				
				<?php else: ?>
					<article>
						<h1><?php _e( 'Sorry, nothing to display.', 'kraftpress' ); ?></h1>
					</article>
				<?php endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</section>
	
<?php get_footer(get_field('site_footer', 'option')); ?>