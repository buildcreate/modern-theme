<!-- sidebar -->
<aside class="sidebar" role="complementary">
	<div class="sidebar-widget">
		<?php if(is_single() && get_post_type() == 'post') { ?>
			<a class="button" style="margin-bottom: 2em; width: 100%; text-align: center; font-size: 1.3em;" href="/blog" title="Back to Blog">Back to Blog &nbsp; &nbsp; <i class="btl bt-angle-right"></i></a>
		<?php } ?>
		
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-1')) ?>
		
	</div>
</aside>
<!-- /sidebar -->