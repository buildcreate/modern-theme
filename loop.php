<?php if(have_posts()) : ?>
	<div class="loop">
		<?php while(have_posts()) : ?>
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<?php if(has_post_thumbnail()) : ?>
					<?php the_post_thumbnail('thumbnail', array('class' => 'featured-left')); ?>
				<?php endif; ?>

				<h2 class="post-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h2>	

				<div class="written-by">Written by: <em><?php the_author() ?></em> on <?php the_time('F jS, Y'); ?></div>

				<div class="user-content">
					<?php the_excerpt(); ?>
				</div>

				<div class="post-meta">
					<div>Find more in: <?php the_category(' | '); ?></div>
					<span class="clearer"></span>
				</div>

			</article>
			
		<?php endwhile; ?>

		<div class="navigation">
	        <div class="alignleft"><?php previous_posts_link('&laquo; Previous'); ?></div>
	        <div class="alignright"><?php next_posts_link('More &raquo;'); ?></div>
	    </div>
	</div>
<?php else: ?>
	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'kraftpress' ); ?></h2>
	</article>
	<!-- /article -->
<?php endif; ?>
