<!-- search -->

<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">

	<input class="search-input" type="search" placeholder="Search" name="s">
	<input type="hidden" name="post_type" value="post" />
	<button class="search-submit" type="submit" role="button"><i class="btb bt-search"></i></button>

</form>

<!-- /search -->