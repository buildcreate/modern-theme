<?php get_header(get_field('site_header', 'option')); ?>

<?php do_action('mo_render_header', $mo_options); ?>
<?php do_action('mo_between_header_content'); ?>

<section id="main" role="main">
	<div class="wrapper">
		<article style="padding:40px 0;">
			<h2>Sorry, the page requested was not found.</h2>
			<p>Maybe one of the pages below will help you find your way.</p>
			<ul>
				<?php wp_nav_menu(array('theme_location' => 'header', 'items_wrap' => '%3$s', 'container'=> false)); ?>
			</ul>
		</article>
	</div>
</section>

<?php get_footer(get_field('site_footer', 'option')); ?>