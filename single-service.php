<?php get_header(get_field('site_header', 'option')); ?>
	
<?php do_action('mo_render_header', $mo_options); ?>
	
	<section id="main" class="single" role="main">
				
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
						<?php the_content(); ?>
						
					</article>
				
					
				<?php endwhile; ?>
				
				<?php else: ?>
					<article>
						<h1><?php _e( 'Sorry, nothing to display.', 'kraftpress' ); ?></h1>
					</article>
				<?php endif; ?>

	</section>
	
<?php get_footer(get_field('site_footer', 'option')); ?>